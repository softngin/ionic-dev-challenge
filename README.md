## Ionic Programming Task
In order to be considered for the Ionic developer position, you must complete the following steps. 

*Note: This task should take no longer than 1-2 hours at the most.*
### Prerequisites
- Please note that this will require some basic [JavaScript](http://www.codecademy.com/tracks/javascript), [AngularJS](https://angularjs.org/), HTML5/CSS3, and [Ionic Framework](http://ionicframework.com/) knowledge. 
## Task
1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create an Ionic application that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [angular/angular.js](https://github.com/angular/angular.js) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Creates a view that groups the recent commits by author in a list layout, one commit per line. This is an [example screenshot](example.jpg) of what each cell should look like. If the commit hash ends in a number, color the background of that row to light blue (#E6F1F6).
	- Add a Refresh button to refresh the list view. This refresh button should be written using CSS properties alone. [Click here](example-button.png) to see an example of the button. The button should be in the center of the screen, below the list view.
3. Commit and Push your code to your fork
4. Send us a pull request and make sure to give us (Bitbucket username dansf) read access to your fork and branch.
5. We should be able to pull you code, build and run the project. If any special build or run instructions are needed please include them in an instructions.txt file.
6. We will review your code and get back to you.
